+++
title = "Interfacing a 102x64 graphic display with an STM32 microcontroller"
date = 2021-09-03
draft = true
+++
![Photo of the graphic LCD display.](display.jpg)

The goal of this post is to show how to establish a connection with the graphic LCD display **EA DOGS102**. This is a graphical low power display with 102x64 pixels. It costs about 20€ and is perfect for battery powered projects. Without a backlight it draws a typical current of 250μA — easily supplied by a coin cell! As the photo shows, the liquid crystal display is a *display on glass* (hence the name DOG, with the size S) where the display and the display driver IC are on a glass plate. This makes the display extremely thin — about 2mm.

To control the display an STM32 microcontroller is used. This example uses the easily available “Blue Pill” microcontroller board shown in the next photo.

![Photo of the STM32 microcontroller board “Blue Pill” with the programmer and debugger SEGGER J-Link Edu Mini.](/images/BluePillBoard.jpg)

The STM32F103C8T6 microcontroller board (the “Blue Pill”) is a popular and cheap microcontroller board similar to Arduino boards. It features a much more powerful 32 Bit STM32 microcontroller on a small board with all the pins available via pin headers. For this project it does not matter which STM32 microcontroller is used as the STM32 HAL (hardware abstraction layer) makes the configuration and use of the communication peripherals independent of the specific microcontroller. The interface to the display is an SPI - serial periphal interface. There is a dedicated hardware peripheral in most STM32 that can be used to easily receive and transmit data over SPI.

## Requirements
For this project, the following hardware is used:

 - “Blue Pill” STM32F103C8T6 board
 - Graphical LCD EA DOGS102
 - 3 1μF capacitors
 - Programmer for the STM32 — SEGGER J-Link Edu Mini
 - Breadboard with jumper cables
 - Logic analyzer or oscilloscope for debugging

Of course a different programmer can be used. On the internet very cheap clones of the ST-Link can be bought.

The firmware for the microcontroller is developed in the STM32CubeIDE from ST Microelectronics. This is the Eclipse IDE modified for STM32 development and integrates the graphical configuration utility STM32CubeMX. This graphical configuration makes prototyping and experimenting effortless and trouble-free.

## Schematic
The needed connections and parts are described well in the display datasheet under section Application Example[^1]:

![Application example from the display datasheet.](display_datasheet_schematic.png)

The display needs 3 decoupling capacitors and a power supply between 2.5V and 3.3V. The Blue Pill has a power supply of 3.3V so the display can be connected directly. To use a microcontroller running with 5V (for example Arduino boards) level shifters have to be used, otherwise the display might be destroyed.

## Microcontroller configuration
Only the SPI module and optionally (depending on your programmer) the debug module have to be activated and configured. Full SPI requires 4 wires: a clock, a chip select (to select a specific chip), master out/slave in (MOSI) and master in/slave out (MISO). As the display does not send any data, the MISO line is not used. *Transmit Only Master* is chosen as for the SPI peripheral mode. The datasheet says SPI mode 3 is used with MSB first and shows the following timing diagram (important for debugging with an oscilloscope or logic analyzer):

![Screenshot of the SPI timing from the display datasheet.](display_datasheet_SPI.png)

Clock polarity means the idle polarity of the clock[^2]. In the diagram the clock line is high until transmission begins, so CPOL is set to High. The clock phase can be first or second edge. There are arrows in the diagram showing the edge when the bit is read from the data line SDA (MOSI). For this display it is the second edge. This results in the following configuration:


<img alt="Screenshot of the SPI peripheral configuration in the STM32CubeIDE." 
src="STM32CubeIDE_SPI_configuration.png" style="width: 50%;margin-left: auto;margin-right: auto; display: inline-block;"><img alt="Screenshot of the GPIO peripheral configuration in the STM32CubeIDE." 
src="STM32CubeIDE_GPIO_configuration.png" style="width: 50%;margin-left: auto;margin-right: auto; display: inline-block;">

![Screenshot of the microcontroller pinout in the STM32CubeIDE.](STM32CubeIDE_pinout.png)

Now just make those connection on the breadboard and don’t forget the 3 1μF capacitors!

<table>
    <thead>
        <tr><th>LCD pin</th><th></th><th>STM32 pin</th><th></th></tr>
    </thead>
    <tbody>
        <tr><td>28</td><td>CS0</td><td>PA4</td><td>SPI1_NSS</td></tr>
        <tr><td>27</td><td>RST</td><td>PB0</td><td>RESET</td></tr>
        <tr><td>26</td><td>CD</td><td>PA6</td><td>CD</td></tr>
        <tr><td>25</td><td>SCK</td><td>SPI1_SCK</td><td>PA5</td></tr>
        <tr><td>24</td><td>SDA</td><td>SPI1_MOSI</td><td>PA7</td></tr>
        <tr><td>23</td><td>V+</td><td>3.3V</td></tr>
        <tr><td>22</td><td>V+</td><td>3.3V</td></tr>
        <tr><td>21</td><td>GND</td><td>GND</td></tr>
        <tr><td>20</td><td>GND</td><td>GND</td></tr>
        <tr><td>19</td><td>VB1+</td><td>1μF capacitor</td></tr>
        <tr><td>18</td><td>VB0+</td><td>1μF capacitor</td></tr>
        <tr><td>17</td><td>VB0-</td><td>1μF capacitor</td></tr>
        <tr><td>16</td><td>VB1-</td><td>1μF capacitor</td></tr>
        <tr><td>15</td><td>VLCD</td><td>1μF capacitor</td></tr>
    </tbody>
</table>

Using jumper cables to make the connections, the breadboard might look something like this:

![Photo of the microcontroller and display connected with jumper cables on a breadboard.](photo_connections.jpg)

## Code
Using the HAL makes the source code very simple. For convenience, a function was created that takes a data byte and sends it via the SPI module. This allows sending the commands in a straightforward way:
```c
//...
void writeCommand(uint8_t cmd) {
    HAL_SPI_Transmit(&hspi1, &cmd, 1, 100);
}

//...
int main(void)
{
    /* Reset of all peripherals, Initializes the Flash interface and Systick. */
    HAL_Init();

    /* Configure the system clock */
    SystemClock_Config();

    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_SPI1_Init();

    // display reset is active during boot
    HAL_Delay(100);
    HAL_GPIO_WritePin(RESET_GPIO_Port, RESET_Pin, 1); // set reset to inactive

    // initialisation sequence
    writeCommand(0x40); // display start line 0
    writeCommand(0xA1); // SEG revers
    writeCommand(0xC0); // normal COM0~COM63
    writeCommand(0xA4); // disable, set all pixel to ON
    writeCommand(0xA6); // display inverse off
    writeCommand(0xA2); // set bias 1/9 (duty 1/65)
    writeCommand(0x2F); // booster, regulator and follower on
    writeCommand(0x27); // set contrast
    writeCommand(0x81); // set contrast
    writeCommand(0x10); // set contrast
    writeCommand(0xFA); // set temperature compensation curve
    writeCommand(0x90); // set temperature compensation curve

    /* Infinite loop */
    while (1)
    {
        writeCommand(0xAF); // display on
        HAL_Delay(1000);
        writeCommand(0xAE); // display on
        HAL_Delay(1000);
    }
}
```
The initialisation sequence is copied from the display datasheet[^1]:

![Screenshot of the initialisation sequence example from the display datasheet.](display_datasheet_initialisation.png)

To check the correct configuration of the SPI module, an oscilloscope was used to measure the clock, the chip select and the data line. Most oscilloscopes and logic analyzers can decode the SPI protocol. This can be seen in the screenshot below where the first part of the initialisation sequence is shown in hexadezimals:

![Oscilloscope screenshot with the SPI lines and the SPI decoder showing the initialisation sequence.](oscilloscope_SPI.png)
<span class="caption">CH1 (yellow): master out, CH2 (blue): chip select, CH3 (red): clock</span>


# Result
As expected, the display toggles between all pixels on and off. Next step is sending data to show on the display!

![Video of the microcontroller and display on a breadboard. The display is inverting its content every second.](display_inverting.gif)

---
[^1]: EA DOGS102-6 datasheet: <https://www.lcd-module.com/eng/pdf/grafik/dogs102-6e.pdf> or <https://www.mouser.de/datasheet/2/127/dogs102_6e-6338.pdf>

[^2]: For more information on SPI, see <https://en.wikipedia.org/wiki/Serial_Peripheral_Interface>