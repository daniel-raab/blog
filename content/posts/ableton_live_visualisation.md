+++
title = "Ableton Live visualisation"
date = "2021-03-30"
draft = true
+++
For the university module “Embedded Systems II” a microcontroller had to be programmed and a term paper be written. The goal of this term paper and the whole module is a deeper understanding of the cross-compilation process and to see underneath the magic of modern IDEs. Instead of pressing a run button that compiles, links and uploads the software to a microcontroller, all the parts have to be understand and then put into a makefile — like in the not-so-olden days.
The hardware is not important: just a Arduino board with some sort of sensor or actuator. My classmate and me chose to develop a visualisation of a potentiometer. 
 A small inexpensive OLED display with 128x64 monochrome pixels is used to visualize parameters from the live music software *Ableton Live* in real time. Goal of the project was programming a microcontroller (here: ATmega328p) without an IDE to gain an understanding of the cross-compiling process.

![A photo of the Arduino and display of the Ableton visualisation project.](/images/ableton_visualisation.jpg)

