+++
title = "Generating images with artificial intelligence: DALL·E 2"
date = 2023-02-15
+++

I am about half a year late to the AI party: [DALL·E 2](https://openai.com/dall-e-2/) was made available in the summer of 2022, but only now did I find time to play around with it.
I read about it on Hacker News and blog articles like [I replaced all our blog thumbnails using DALL·E 2 for $45: here’s what I learned](https://deephaven.io/blog/2022/08/08/AI-generated-blog-thumbnails/>) and recently a friend renewed my interest.
So I used all my free credits and then some playing around.
Here are the results that I like the most (you can see the prompt by hovering with the mouse):

<div class="grid">
	<div><img src="DALL·E 2023-02-15 17.45.15 - A victorian hunting dog smoking a pipe, in the style of Rembrandt van Rijn.png" title="A victorian hunting dog smoking a pipe, in the style of Rembrandt van Rijn" /></div>
	<div><img src="DALL·E 2023-02-15 16.25.27 - A victorian hunting dog smoking a pipe, in the style of Rembrandt van Rijn.png" title="A victorian hunting dog smoking a pipe, in the style of Rembrandt van Rijn"/></div>
</div>

<div class="grid">
	<div><img src="DALL·E 2023-02-15 14.41.18 - retro tlr camera.png" title="retro tlr camera"/></div>
	<div><img src="DALL·E 2023-02-15 14.42.10 - art nouveau retro tlr camera.png" title="art nouveau retro tlr camera"/></div>
	<div><img src="DALL·E 2023-02-15 14.42.28.png" title="art nouveau retro tlr camera — variation" /></div>
</div>


<div class="grid">
	<div><img src="DALL·E 2023-02-15 14.43.14 - retro tlr camera as a art art nouveau poster.png" title="retro tlr camera as a art art nouveau poster" /></div>
	<div><img src="DALL·E 2023-02-15 14.43.56.png" title="retro tlr camera as a art art nouveau poster.png" title="retro tlr camera as a art art nouveau poster — variation" /></div>
	<!-- <div><img src="DALL·E 2023-02-15 14.43.52.png" /></div> -->
	<div><img src="DALL·E 2023-02-15 14.43.47.png" title="retro tlr camera as a art art nouveau poster.png" title="retro tlr camera as a art art nouveau poster — variation" /></div>
	<div><img src="DALL·E 2023-02-15 14.43.42.png" title="retro tlr camera as a art art nouveau poster.png" title="retro tlr camera as a art art nouveau poster — variation" /></div>
</div>


<div class="grid">
	<div><img src="DALL·E 2023-02-15 14.44.34 - monstera plant art noveau poster.png" title="monstera plant art noveau poster" /></div>
	<div><img src="DALL·E 2023-02-15 21.54.49 - a brown cocker spaniel art noveau poster.png" title="a brown cocker spaniel art noveau poster" /></div>
</div>


<div class="grid">
	<div><img src="DALL·E 2023-02-15 14.44.39 - monstera plant art noveau poster.png" title="monstera plant art noveau poster" /></div>
	<div><img src="DALL·E 2023-02-15 14.44.41 - monstera plant art noveau poster.png" title="monstera plant art noveau poster" /></div>
	<div><img src="DALL·E 2023-02-15 14.44.45 - monstera plant art noveau poster.png" title="monstera plant art noveau poster" /></div>
</div>

<div class="grid">
	<div><img src="DALL·E 2023-02-15 14.18.14 - pine cone, art nouveau poster.png" title="pine cone, art nouveau poster" /></div>
	<div><img src="DALL·E 2023-02-15 14.18.23 - pine cone, art nouveau poster.png" title="pine cone, art nouveau poster" /></div>
	<div><img src="DALL·E 2023-02-15 14.18.29 - pine cone, art nouveau poster.png" title="pine cone, art nouveau poster" /></div>
</div>



<div class="grid">
	<div><img src="DALL·E 2023-02-15 15.25.12 - greyhound watercolor.png" title="greyhound watercolor" /></div>
	<div><img src="DALL·E 2023-02-15 15.21.27 - baby astronaut, pencil and watercolor.png" title="baby astronaut, pencil and watercolor" /></div>
</div>

<div class="grid">
	<div><img src="DALL·E 2023-02-15 15.13.08 - dog with baby watercolor.png" title="dog with baby watercolor" /></div>
	<div><img src="DALL·E 2023-02-15 15.18.56 - dog running with baby boy watercolor.png" title="dog running with baby boy watercolor" /></div>
	<div><img src="DALL·E 2023-02-15 15.18.53 - dog running with baby boy watercolor.png" title="dog running with baby boy watercolor" /></div>
	<div><img src="DALL·E 2023-02-15 15.09.56 - cocker spaniel watercolor.png" title="cocker spaniel watercolor" /></div>
</div>


<div class="grid">
	<div><img src="DALL·E 2023-02-15 14.38.01 - grey hound, pixel art.png" title="grey hound, pixel art" /></div>
	<div><img src="DALL·E 2023-02-15 14.37.14 - dachshund, pixel art.png" title="dachshund, pixel art" /></div>
</div>

After enough prompts I learned that it is good to specify an art style.
The less realistic the style the better for DALL·E because the small errors will not matter too much.
And generating human faces is still hard and often images will land in the ["uncanny valley" and become creepy](https://youtu.be/PEikGKDVsCc).
Without a specific art style, the results are often very cartoony:

![Screenshot of the DALL·E prompt with corgis in space](Screenshot_corgis_in_space.png)

So a prompt for the style of Vincent van Gogh works quite well:

![Screenshot of the DALL·E prompt with cocker spaniels by Vincent van Gogh](screenshot_cocker_spaniel_van_gogh.png)


<!-- 
## Funny Greyhound

<div><img src="DALL·E 2023-02-15 15.12.21 - greyhound as a cyclist.png" /></div>
<div class="grid">
	<div><img src="DALL·E 2023-02-15 15.11.31 - hipster greyhound.png" /></div>
	<div><img src="DALL·E 2023-02-15 15.11.36 - hipster greyhound.png" /></div>
	<div><img src="DALL·E 2023-02-15 15.11.50 - hipster greyhound poster.png" /></div>
	<div><img src="DALL·E 2023-02-15 15.11.55 - hipster greyhound poster.png" /></div>
	<div><img src="DALL·E 2023-02-15 15.11.59 - hipster greyhound poster.png" /></div>
</div>

## Leipzig

<div><img src="DALL·E 2023-02-15 15.09.08 - leipzig in art noveau poster.png" /></div> -->

