+++
title = "Hello, World! Zig on a STM32"
date = 2021-12-12
draft = true
+++

After reading a few blog posts about Zig (<https://kevinlynagh.com/rust-zig/>, <https://scattered-thoughts.net/writing/assorted-thoughts-on-zig-and-rust/>) I was intruiged to give this new programming language a spin. It is marketed as a „better C” — the same low-level access that makes C the standard in embedded programming but without the footguns (see ) and the design with the experience of the last decades of C programming.

Using Zig instead of C means also developing a greater understanding of behind the scenes stuff like linking and assembler code. Understanding the output of the compiler (be it Zig code oder C code) is something I am working on.

