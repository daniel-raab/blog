+++
title = "Weekend Project: Tiny Biped Robot"
date = "2019-12-11"
draft = true
+++

# Weekend Project: Tiny Biped Robot


<video height=400px controls>
	<source src="/videos/TAMI1.mp4">
</video>
<video height=400px controls>
	<source src="/videos/TAMI2.mp4">
</video>