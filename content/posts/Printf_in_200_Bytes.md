+++
title = "Printf in 200 Bytes"
date = 2023-02-13
draft = true
+++

 - Microcontroller with 8 kB flash and 2 kB RAM
 - stdlib (newlib nano) -> suddenly 6 kB of flash are used 
 - replace printf with formatting on host, send string and args to host
 - problem: varargs still uses a lot of flash
 - replace varargs with vararg macro

```c
#define GET_MACRO(_0, _1, _2, _3, _4, _5, NAME, ...) NAME
#define print(...) GET_MACRO(__VA_ARGS__, printf5, printf4, printf3, printf2, printf1, prints)(__VA_ARGS__)

void prints(char *str);

void printf1(char *fmt, int v1);
void printf2(char *fmt, int v1, int v2);
void printf3(char *fmt, int v1, int v2, int v3);
void printf4(char *fmt, int v1, int v2, int v3, int v4);
void printf5(char *fmt, int v1, int v2, int v3, int v4, int v5);
```

```c
// TODO
```

```
Memory region         Used Size  Region Size  %age Used
             RAM:         324 B         2 KB     15.82%
           FLASH:        2172 B         8 KB     26.51%
```

```
Memory region         Used Size  Region Size  %age Used
             RAM:         324 B         2 KB     15.82%
           FLASH:        2520 B         8 KB     30.76%
```

---
[^1]: <https://stackoverflow.com/a/11763277>