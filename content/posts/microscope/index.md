+++
title = "A look into an integrated circuit"
date = 2021-12-25
draft = true
+++
<br>
<img src="microscope.jpg" alt="Photo of the microscope from my childhood." style="height: 20em; width:  auto ;margin-left: auto;margin-right: auto; display: block">

Recently, I rediscovered my fascination for microscopic life. Water bears, also called tardigrades, algae or bacteria — all too small to see with the naked eye. A binge of all the videos of the Youtube channel [Journey to the Microcosmos](https://www.youtube.com/c/microcosmos) was necessary.

At some point as a child, I got a microscope as a present. This christmas I discovered that it still exists and you can see it in the image above. Of course, I immediately checked that it still worked. I grabbed some moss from outside my window and melted the ice. A drop of that water revealed to be quite alive! The microscope is quite dirty, but still:

<video src="moss.mp4" poster="moss_cover.png" controls loop></video>

This gave me an idea to look at stuff I work with every day: integrated circuits. I have seen what the die inside an integrated circuit looks like, but I wanted to open a chip myself and then see myself. But how to remove the die from an IC? I do not have the necessary chemicals or precision tools for this at home. After finding this video showing how the die is easily removed with heat, I thought this might be an easy DIY way. At home, I do not have a heatgun. But fire reaches a few hundred degrees easily, so maybe a candle or lighter might work? It does:

![Photo of a broken IC](broken_IC.jpg)

I have a few cheap opamps (LM324) in DIP lying around that I do not mind breaking. After breaking apart the first IC, I promptly lost the die. With the second IC I have more luck:

![A photo of the die on top a 1 cent coin.](die_on_a_cent.jpg)

As you can see, the die is very small: a square with about 0.5mm width. It is so thin, that light can shine through. This makes it possible to look at with a simple microscope (where the light is supposed to pass the sample).

![Microscope image of the whole die.](whole_die.png)
![Microscope image of a part of the die.](die_zoomed.png)