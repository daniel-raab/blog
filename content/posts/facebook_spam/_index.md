+++
title = "Daten von über 500 Millionen Facebook-Nutzern frei verfügbar"
date = 2021-12-12
draft = true
+++
Anfang April 2021 wurde bekannt, dass die Telefonnummern, E-Mail-Adressen und andere Daten von über 500 Millionen Facebook-Nutzern im Internet aufgetaucht sind. „Daten hunderter Millionen Facebook-Nutzer erneut im Netz entdeckt“ auf [heise.de](https://www.heise.de/news/Daten-hunderter-Millionen-Facebook-Nutzer-erneut-im-Netz-entdeckt-6005192.html). Ein paar Tage nach diesen Nachrichten bekam ich die folgende SMS auf mein Smartphone:
![SMS: ](/sms1.jpg)
Also auf den Link gedrückt und auf der DHL-Seite gelandet, aber hoppla — eine „Applikation“ herunterladen?
![SMS: ](/link1.jpg)
Erst jetzt werde ich misstrauisch, mir fällt nichts ein, das ich bestellt hätte und die Adresse ist definitiv nicht DHL. Die SMS hat Rechtschreibfehler und sagt, dass es die letzte Chance wäre bevor das Paket zurückgesendet wird. Also Spam und Viren. Was hat das Ganze mit Facebook zu tun? Erstmal nichts, aber als meine Freundin am nächsten Tag dann eine ähnliche SMS bekommt werde ich stutzig. Das kann doch kein Zufall sein, da fällt mir das Datenleck bei Facebook sein. Schnell überprüfen lässt es sich auch, denn es gibt ja die Webseite <https://haveibeenpwned.com/> und siehe da, von uns beiden wurden die Telefonnummern mit veröffentlicht. Das ist natürlich schön einfach die frei verfügbaren Daten von Facebook zu schnappen, nach Ländercodes zu sortieren und dann entsprechende Phishing-Webseiten zu erstellen! Während des Schreibens kommt noch eine zweite SMS:
![SMS: ](/sms2.jpg)
![SMS: ](/link2.jpg)