+++
title = "NFC Card: Hello, World!"
date = "2021-04-11"
description = "TODO"
Image = "/images/NFC_Card_blinking_hello_world.gif"
draft = true
+++

![The NFC business card is blinking its “Hello, World!”](/images/NFC_Card_blinking_hello_world.gif)

For the university semester break I wanted to do a hands-on project that gives me a break from the theory-laden first Masters semester. I chose to think up a few ideas that could be powered via NFC and fit on a credit card sized PCB. I designed, ordered and soldered the PCB and now I am starting with the software of the project — “Hello, World!” says the LED!