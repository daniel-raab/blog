+++
title = "Arduino Project: Camera Time Lapse"
date = "2014-06-10"
draft = true
+++

<p style="text-align:center;">
	<video width=500px controls>
		<source src="/videos/timelapse.webm">
		<source src="/videos/timelapse.m4v">
	</video>
</p>