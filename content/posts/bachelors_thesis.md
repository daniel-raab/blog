+++
title = "Bachelor’s Thesis: Industrial NFC sensor"
date = "2020-04-06"
draft = true
+++

German Title: *„Entwicklung eines Funktionsmusters zur Messdatenübertragung und Energy Harvesting via NFC“*. Translation: *“Development of a proof of concept prototype for the transmission of sensor data and energy harvesting via NFC”*. 

I cannot publish the thesis as the intellectual property belongs to my employers and it is protected by a non-disclosure agreement.

![Cover page of my Bachelor’s thesis.](/images/Bachelorarbeit_Titelseite.png)