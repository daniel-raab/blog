+++
title = "Handheld Electrocardiograph"
date = "2019-12-31"
draft = true
+++

![Photo of the finished electrocardiograph](EKG.jpg)

To complete my studies with a Bachelor’s degree a 3 month internship is required. This can be done either in the labs of the university or a business. I chose to do my internship in the medical engineering lab and got the task to built a electrocardiograph for demonstration purposes. An electrocardiograph is the machine in the hospital that shows your heart activity — you probably know it from television — it’s the one beeping and showing a flat line when someone has died.

## Principle of an electrocardiogram


## The goal
My assignment was to build a portable, handheld electrocardiograph that can be used in lectures and classes to teach people about medical engineering. So the following goals were formulated:

 - portable design powered by a battery
 - graphic display for displaying the electrocardiogram
 - two electrode system with stainless steel electrodes
 - acoustic signaling of heart beats
 - measurement of the heart rate
 - design of an ergonomic casing

## Experimenting
The heavy lifting is being done by a specialised ECG chip, that
![](test_setup.jpg)
![](test_setup.mp4)

## Result
![Photo of the finished electrocardiograph.](EKG.jpg)
![](EKG_demonstration.mp4)

If you want to read more (and can read German), here is the documentation of the project as a PDF: 
 - [Documentation (German, 11.9MB)](Praxisprojekt_Raab.pdf)
 - [Addendum (German, 800kB)](Praxisprojekt_Nachtrag.pdf).