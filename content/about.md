+++
title = "About"
description = "About me"
date = 2023-02-13
+++

<div>
<img src="/images/dall-e-selfie.png" style="float: right;height: 250px;margin: 0 0 16px 16px" alt="Portrait in Art Nouveau style, generated by DALL·E 2" title="Portrait in Art Nouveau style, generated by DALL·E 2"/>

Hi, my name is Daniel and I am an embedded engineer.
I also love to take photographs, both digital and analog.
On this blog I share thoughts and ideas about embedded projects as well as some of my photographs.

</div>

At the moment I am working on my Master's thesis at the HTWK Leipzig.
It is about doing automated testing and continuous integration with embedded systems.
The title (in german) is:

> *Automatisertes Testen von eingebetteter Software mit Hardware-in-the-loop Test*

But outside of work and university I try to find time for personal projects.
Some of them can be found on my [github profile](https://github.com/daniel-raab).

If you want, you can contact me at <daniel.raab@mailbox.org>.
