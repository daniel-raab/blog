+++
title = "Photography"
date = 2022-08-28
template = "photos.html"

[extra]
	images = [
		"/photos/dog_and_baby.jpg",
		"/photos/bücherregal.jpg",
		"/photos/svenja_ascheberg2.jpg",
		"/photos/naumburg_fenster.jpg",
		"/photos/bayern_löwenzahn.jpg",
		"/photos/john_strand.jpg",
		"/photos/fassaden_holland.jpg",
		"/photos/john_holland.jpg",
		"/photos/mast.jpg",
		"/photos/john_wintergarten2.jpg",
		"/photos/warnemünde_dünen.jpg",
		"/photos/hotel_neptun.jpg",
		"/photos/architektur_hamm.jpg",
		"/photos/john_homeoffice.jpg",
		"/photos/svenja_maza_pita.jpg",
		"/photos/blume.jpg",
		"/photos/erzgebirge_wald.jpg",
		"/photos/erzgebirge_tannen.jpg",
		"/photos/john_wintergarten.jpg",
		"/photos/svenja_ascheberg.jpg",
	]
+++